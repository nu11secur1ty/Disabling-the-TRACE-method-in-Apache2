# Apache: Disable the HTTP TRACE Method
![](https://github.com/nu11secur1ty/Disabling-the-TRACE-method-in-Apache2/blob/master/Cover/Cover_.png)


# ABOUT HTTP TRACE

'TRACE' is a HTTP request method used for debugging which echo's back input back to the user. Jeremiah Grossman from Whitehatsec [posted a paper outlining](http://www.cgisecurity.com/whitehat-mirror/WH-WhitePaper_XST_ebook.pdf) a risk allowing an attacker to steal information including Cookies, and possibly website credentials. http://www.apacheweek.com suggests the following solution to disable the HTTP TRACE method by using mod_rewrite.

"TRACE requests can be disabled by making a change to the Apache server configuration. Unfortunately it is not possible to do this using the Limit directive since the processing for the TRACE request skips this authorisation checking. Instead the following lines can be added which make use of the mod_rewrite module.

RewriteEngine On
RewriteCond %{REQUEST_METHOD} ^TRACE
RewriteRule .* - [F]" - www.apacheweek.com

Additional information can be found at the links below.

* http://www.apacheweek.com/issues/03-01-24#news
* [w3 HTTP Protocol Specification](https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html)

-----------------------------------------------------------------------------------------------------------

# Introduction

By default, Apache2 supports the HTTP TRACE method, which could expose your server to certain Cross-Site Scripting attacks.1 In this tutorial, I will show you how to check for TRACE support on your Apache2 server using curl, and then switch it off if it is enabled.


# Testing for TRACE support with curl

- To see if TRACE is supported by your server, you can use curl

```
$ curl -i -X TRACE http://www._website_.com/
HTTP/1.1 200 OK
Date: Wed, 13 Feb 2013 14:22:56 GMT
Server: Apache/2.2.15 (CentOS)
Transfer-Encoding: chunked
Content-Type: message/http
 
TRACE / HTTP/1.1
User-Agent: curl/7.21.7 (x86_64-redhat-linux-gnu) libcurl/7.21.7 NSS/3.13.3.0 zlib/1.2.5 libidn/1.22 libssh2/1.2.7
Host: www._website_.com
Accept: */*
```
As you can see, I am getting a response from the server for the TRACE request. Now let us disable it.

# Disabling TRACE support in Apache2


To switch off TRACE support, you need to open your main Apache2 configuration file which is here on my CentOS box:

```
- /etc/httpd/conf/httpd.conf
- /etc/apache2/httpd.conf
- It depends what operating system of Linux you use
```

Now add this directive to that file (You can added it to the bottom of the file)

```
TraceEnable off
```

restart Apache2:

```
$ service httpd restart
```


Now when you run the same curl command again from your client machine, this will the response which you see:

```
$ curl -i -X TRACE http://www._website_.com/
HTTP/1.1 405 Method Not Allowed
Date: Wed, 13 Feb 2013 14:30:32 GMT
Server: Apache/2.2.15 (CentOS)
Allow: 
Content-Length: 223
Content-Type: text/html; charset=iso-8859-1
 
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>405 Method Not Allowed</title>
</head><body>
<h1>Method Not Allowed</h1>
<p>The requested method TRACE is not allowed for the URL /.</p>
</body></html>
```

# Have fun with nu11secur1ty =)
















